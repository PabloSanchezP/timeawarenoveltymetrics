/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.novelty.temporal;

import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.FreshnessMetricNorm;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.MetricScheme;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.ranksys.formats.parsing.Parser;

/**
 * Utility class to compute statistics about the timestamps.
 *
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 * @param <U>
 *            type of users
 * @param <I>
 *            type of items
 */
public class TimestampCalculator<U, I> {

	// Based on timeStamps
	private Map<I, Double> itemTimesFirst;
	private Map<I, Double> itemTimesAverage;
	private Map<I, Double> itemTimesMedian;
	private Map<I, Double> itemTimesLast;

	// TimeStamp
	private double minTimeStamp;
	private double maxTimeStamp;

	// Relase date
	private double minRelease;
	private double maxRelease;

	// Based on release date
	private Map<I, Double> itemReleaseTime;

	/**
	 * Empty constructor.
	 */
	public TimestampCalculator() {
		itemTimesFirst = new HashMap<>();
		itemTimesLast = new HashMap<>();
		itemTimesAverage = new HashMap<>();
		itemTimesMedian = new HashMap<>();
		itemReleaseTime = new HashMap<>();
		minTimeStamp = Double.MAX_VALUE;
		maxTimeStamp = Double.MIN_VALUE;
		minRelease = Double.MAX_VALUE;
		maxRelease = Double.MIN_VALUE;

	}

	/**
	 * Method that compute the statistics based on the interaction times provided in
	 * the given file.
	 *
	 * @param filePreferences
	 *            the file where the preferences are included
	 * @param ip
	 *            parser for the items
	 * @param characterSplit
	 *            character denoting the field separator
	 * @param columnItem
	 *            column where the items are located in the file
	 * @param columnTimestamp
	 *            column where the timestamps are located in the file
	 * @throws IOException
	 *             when the file cannot be read
	 */
	public void computeInteractionTimes(String filePreferences, Parser<I> ip, String characterSplit, int columnItem,
			int columnTimestamp) throws IOException {
		// Each item is associated with a list of timestamps
		final Map<I, List<Long>> mapItems = new HashMap<>();

		try (Stream<String> stream = Files.lines(Paths.get(filePreferences))) {
			stream.forEach(line -> {
				String[] data = line.split(characterSplit);
				String item = data[columnItem];
				Long time = Long.parseLong(data[columnTimestamp]);
				if (mapItems.get(ip.parse(item)) == null) {
					mapItems.put(ip.parse(item), new ArrayList<>());
				}
				mapItems.get(ip.parse(item)).add(time);

			});
		}

		// Compute maximum, minimum, average and median.
		mapItems.keySet().forEach(item -> {
			double min = mapItems.get(item).stream().mapToLong(l -> l).min().getAsLong();
			itemTimesFirst.put(item, min);
			if (minTimeStamp > min) {
				minTimeStamp = min;
			}

			double max = mapItems.get(item).stream().mapToLong(l -> l).max().getAsLong();
			itemTimesLast.put(item, max);
			if (maxTimeStamp < max) {
				maxTimeStamp = max;
			}

			double avg = mapItems.get(item).stream().mapToLong(l -> l).average().getAsDouble();
			itemTimesAverage.put(item, avg);

			double median = medianTimestamps(mapItems.get(item));
			itemTimesMedian.put(item, median);
		});
	}

	/**
	 * Method that compute the statistics based on the metadata times provided in
	 * the given file.
	 *
	 * @param file
	 *            the file where the preferences are included
	 * @param ip
	 *            parser for the items
	 * @param characterSplit
	 *            character denoting the field separator
	 * @param columnItem
	 *            column where the items are located in the file
	 * @param columnTimestamp
	 *            column where the timestamps are located in the file
	 * @throws IOException
	 *             when the file cannot be read
	 */
	public void computeMetadataTimes(String file, Parser<I> ip, String characterSplit, int columnItem,
			int columnTimestamp) throws IOException {
		try (Stream<String> stream = Files.lines(Paths.get(file))) {
			stream.forEach(line -> {
				String[] data = line.split(characterSplit);
				String item = data[columnItem];

				// Release date, in years (for example 1995, 1996)
				Double relDate = Double.parseDouble(data[columnTimestamp]);
				itemReleaseTime.put(ip.parse(item), relDate);

				if (maxRelease < relDate) {
					maxRelease = relDate;
				}

				if (minRelease > relDate) {
					minRelease = relDate;
				}
			});
		}
	}

	/**
	 * Method that computes the median of a list of timestamps.
	 *
	 * @param times
	 *            the timestamps (not necessarily sorted)
	 * @return the median
	 */
	public static double medianTimestamps(List<Long> times) {
		Long[] toArr = new Long[times.size()];
		toArr = (Long[]) times.toArray(toArr);
		Arrays.sort(toArr);
		if (toArr.length % 2 == 0) {
			return ((double) toArr[toArr.length / 2] + (double) toArr[toArr.length / 2 - 1]) / 2.0;
		} else {
			return (double) toArr[toArr.length / 2];
		}
	}

	/**
	 * An utility method to load already pre-computed values for each metric
	 * variation.
	 *
	 * @param mapSchemeFile
	 *            a map with the name of a file for each metric variation
	 * @param ip
	 *            the parser for items
	 */
	public void load(Map<MetricScheme, String> mapSchemeFile, Parser<I> ip) {
		mapSchemeFile.forEach((s, f) -> loadSpecificMap(f, getSchemeMap(s), ip));
		if (!this.itemTimesFirst.isEmpty()) {
        	this.itemTimesFirst.forEach((key, value) -> {
        		if (this.minTimeStamp > value)
        			this.minTimeStamp = value;
        	});
        }
        
        if (!this.itemTimesLast.isEmpty()) {
        	this.itemTimesLast.forEach((key, value) -> {
        		if (this.maxTimeStamp < value)
        			this.maxTimeStamp = value;
        	});
        }
	}
	
	

	/**
	 * An utility method that saves the computed values for each metric variation
	 * for later use.
	 *
	 * @param mapSchemeFile
	 *            a map with the name of a file for each metric variation
	 */
	public void save(Map<MetricScheme, String> mapSchemeFile) {
		mapSchemeFile.forEach((s, f) -> {
			try (PrintStream out = new PrintStream(f)) {
				getSchemeMap(s).forEach((k, v) -> out.println(k.toString() + "\t" + v));
			} catch (FileNotFoundException ex) {
				ex.printStackTrace();
			}
		});
	}

	/**
	 * A method that loads a single map.
	 *
	 * @param <I>
	 *            the type of items
	 * @param file
	 *            the file to be read (assuming the items are in the first column
	 *            and the metric value in the second)
	 * @param map
	 *            the map where the values will be loaded
	 * @param ip
	 *            the parser for items
	 */
	private static <I> void loadSpecificMap(String file, Map<I, Double> map, Parser<I> ip) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] data = line.split("\t");
				map.put(ip.parse(data[0]), Double.parseDouble(data[1]));
			}
			br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * A method where the appropriate map is given based on a metric variation.
	 *
	 * @param scheme
	 *            the variation of the metric
	 * @return the map with a value for each item
	 */
	public Map<I, Double> getSchemeMap(MetricScheme scheme) {
		switch (scheme) {
		case AVG:
			return itemTimesAverage;
		case FIRST:
			return itemTimesFirst;
		case LAST:
			return itemTimesLast;
		case MEDIAN:
			return itemTimesMedian;
		case FIRST_RELEASE:
			return itemReleaseTime;
		default:
			return null;
		}
	}

	/**
	 * The score of an item depending on the normalization and metric variation.
	 *
	 * @param i
	 *            the item
	 * @param norm
	 *            the normalization
	 * @param scheme
	 *            the metric variatio
	 * @return the score
	 */
	public Double getScore(I i, FreshnessMetricNorm norm, MetricScheme scheme) {
		final Map<I, Double> map = getSchemeMap(scheme);
		double min = minTimeStamp;
		double max = maxTimeStamp;
		if (scheme.equals(MetricScheme.FIRST_RELEASE)) {
			min = minRelease;
			max = maxRelease;
		}
		switch (norm) {
		case MINMAXNORM:
			return (map.get(i) - min) / (max - min);
		default:
			return (map.get(i) / max);
		}
	}
}
