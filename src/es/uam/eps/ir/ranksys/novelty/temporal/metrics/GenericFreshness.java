/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.novelty.temporal.metrics;

import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.novdiv.itemnovelty.ItemNovelty;
import es.uam.eps.ir.ranksys.novdiv.itemnovelty.metrics.ItemNoveltyMetric;

/**
 * An ItemNoveltyMetric model to instantiate the abstract class. It has no own
 * methods.
 * 
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 * @param <U>
 *            type of users
 * @param <I>
 *            type of items
 */
public class GenericFreshness<U, I> extends ItemNoveltyMetric<U, I> {

	/**
	 * Constructor needed because ItemNoveltyMetric is abstract
	 * 
	 * @param cutoff
	 *            the cutoff of this metric
	 * @param novelty
	 *            the novelty model
	 * @param relevanceModel
	 *            the relevance model
	 * @param disc
	 *            the ranking discount model
	 */
	public GenericFreshness(int cutoff, ItemNovelty<U, I> novelty, RelevanceModel<U, I> relevanceModel,
			RankingDiscountModel disc) {
		super(cutoff, novelty, relevanceModel, disc);
	}
}
