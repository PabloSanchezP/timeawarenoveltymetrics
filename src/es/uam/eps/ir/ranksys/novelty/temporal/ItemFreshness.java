/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.novelty.temporal;

import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.novdiv.itemnovelty.ItemNovelty;
import es.uam.eps.ir.ranksys.novdiv.itemnovelty.ItemNovelty.UserItemNoveltyModel;
import it.unimi.dsi.fastutil.objects.Object2DoubleMap;
import it.unimi.dsi.fastutil.objects.Object2DoubleOpenHashMap;

/**
 *
 * Generic implementation of the freshness metric that integrates several
 * novelty models.
 *
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 * @param <U>
 *            type of users
 * @param <I>
 *            type of items
 */
public class ItemFreshness<U, I> extends ItemNovelty<U, I> implements UserItemNoveltyModel<U, I> {

	/**
	 * Enum for the normalization strategy.
	 */
	public enum FreshnessMetricNorm {
		NO_NORM, MINMAXNORM
	};

	/**
	 * Enum for the types of freshness metrics.
	 */
	public enum MetricScheme {
		FIRST, LAST, AVG, MEDIAN, FIRST_RELEASE
	}

	private final Object2DoubleMap<I> itemNovelty;

	/**
	 * Constructor for a generic freshness metric.
	 *
	 * @param recommenderData
	 *            the data with the recommendations generated
	 * @param timeCalc
	 *            an utility class to make operations with timestamps (it should
	 *            have already been initialized)
	 * @param norm
	 *            a normalization
	 * @param scheme
	 *            a metric variation
	 */
	public ItemFreshness(PreferenceData<U, I> recommenderData, TimestampCalculator<?, I> timeCalc,
			FreshnessMetricNorm norm, MetricScheme scheme) {
		super();
		itemNovelty = new Object2DoubleOpenHashMap<>();
		itemNovelty.defaultReturnValue(1.0);
		recommenderData.getItemsWithPreferences().forEach(i -> {
			itemNovelty.put(i, timeCalc.getScore(i, norm, scheme));
		});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double novelty(I i) {
		return itemNovelty.getDouble(i);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected UserItemNoveltyModel<U, I> get(U t) {
		return this;
	}
}
