/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.rec;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/**
 * *
 * Skyline recommender.
 *
 * It recommends the items rated by the user in test. This recommender should
 * only be used to analyze the highest values a recommender may achieve for
 * relevance metrics.
 *
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 *
 * @param <U> type of users
 * @param <I> type of items
 */
public class SkylineRecommender<U, I> extends FastRankingRecommender<U, I> {

    protected final FastPreferenceData<U, I> dataTest;

    public SkylineRecommender(FastPreferenceData<U, I> dataTest) {
        super(dataTest, dataTest);
        this.dataTest = dataTest;
    }

    @Override
    public Int2DoubleMap getScoresMap(int uidx) {
        Int2DoubleOpenHashMap scoresMap = new Int2DoubleOpenHashMap();
        scoresMap.defaultReturnValue(0.0);

        this.dataTest.getUidxPreferences(uidx).forEach(i -> {
            scoresMap.put(i.v1, i.v2);
        });

        return scoresMap;
    }
}
