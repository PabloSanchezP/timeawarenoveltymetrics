/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.rec;

import java.util.Map;

import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.MetricScheme;
import es.uam.eps.ir.ranksys.novelty.temporal.TimestampCalculator;
import es.uam.eps.ir.ranksys.rec.fast.FastRankingRecommender;
import it.unimi.dsi.fastutil.ints.Int2DoubleMap;
import it.unimi.dsi.fastutil.ints.Int2DoubleOpenHashMap;

/**
 * *
 * Freshness Skyline recommender.
 *
 * Recommender that will return the most fresh items according to a specific
 * timestampCalculator scheme.
 *
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 *
 * @param <U> type of users
 * @param <I> type of items
 */
public class SkylineFreshnessRecommender<U, I> extends FastRankingRecommender<U, I> {

    protected final FastPreferenceData<U, I> dataTrain;
    protected final TimestampCalculator<U, I> timeCalculator;
    protected final MetricScheme scheme;
    protected Int2DoubleOpenHashMap genericMap;

    public SkylineFreshnessRecommender(FastPreferenceData<U, I> dataTrain, TimestampCalculator<U, I> timeCalculator,
            MetricScheme scheme) {
        super(dataTrain, dataTrain);
        this.dataTrain = dataTrain;
        this.timeCalculator = timeCalculator;
        this.scheme = scheme;

        Map<I, Double> mapFreshness = timeCalculator.getSchemeMap(scheme);
        genericMap = new Int2DoubleOpenHashMap();
        genericMap.defaultReturnValue(0.0);
        for (I item : mapFreshness.keySet()) {
            genericMap.put(this.item2iidx(item), (double) mapFreshness.get(item));
        }
    }

    @Override
    public Int2DoubleMap getScoresMap(int uidx) {
        return genericMap;
    }

}
