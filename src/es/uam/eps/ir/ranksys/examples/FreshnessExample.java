/*******************************************************************************
 * Copyright (C) 2018 Pablo Sanchez, Information Retrieval Group at Universidad Autónoma de Madrid, http://ir.ii.uam.es
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package es.uam.eps.ir.ranksys.examples;

import es.uam.eps.ir.ranksys.novelty.temporal.metrics.GenericFreshness;
import es.uam.eps.ir.ranksys.novelty.temporal.TimestampCalculator;
import static org.ranksys.formats.parsing.Parsers.lp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jooq.lambda.tuple.Tuple2;
import org.jooq.lambda.tuple.Tuple4;
import org.ranksys.formats.parsing.Parser;
import org.ranksys.formats.preference.SimpleRatingPreferencesReader;
import org.ranksys.formats.rec.RecommendationFormat;
import org.ranksys.formats.rec.SimpleRecommendationFormat;

import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.FreshnessMetricNorm;
import es.uam.eps.ir.ranksys.novelty.temporal.ItemFreshness.MetricScheme;
import es.uam.eps.ir.ranksys.core.Recommendation;
import es.uam.eps.ir.ranksys.core.preference.ConcatPreferenceData;
import es.uam.eps.ir.ranksys.core.preference.PreferenceData;
import es.uam.eps.ir.ranksys.core.preference.SimplePreferenceData;
import es.uam.eps.ir.ranksys.fast.index.FastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.FastUserIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastItemIndex;
import es.uam.eps.ir.ranksys.fast.index.SimpleFastUserIndex;
import es.uam.eps.ir.ranksys.fast.preference.FastPreferenceData;
import es.uam.eps.ir.ranksys.fast.preference.SimpleFastPreferenceData;
import es.uam.eps.ir.ranksys.metrics.RecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.SystemMetric;
import es.uam.eps.ir.ranksys.metrics.basic.AverageRecommendationMetric;
import es.uam.eps.ir.ranksys.metrics.basic.NDCG;
import es.uam.eps.ir.ranksys.metrics.basic.Precision;
import es.uam.eps.ir.ranksys.metrics.rank.NoDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rank.RankingDiscountModel;
import es.uam.eps.ir.ranksys.metrics.rel.BinaryRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.NoRelevanceModel;
import es.uam.eps.ir.ranksys.metrics.rel.RelevanceModel;
import es.uam.eps.ir.ranksys.rec.Recommender;
import es.uam.eps.ir.ranksys.rec.SkylineFreshnessRecommender;
import es.uam.eps.ir.ranksys.rec.SkylineRecommender;
import es.uam.eps.ir.ranksys.rec.fast.basic.PopularityRecommender;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Example program to test the freshness metric. It also generates a recommender
 * if there is no file available.
 *
 * @author Pablo Sanchez Perez <pablo.sanchezp@estudiante.uam.es>
 */
public class FreshnessExample {

    public static void main(String[] args) throws Exception {
        if (args.length < 7) {
            System.out.println("Usage: training_file test_file cutoff rec_file flag_complete_index rel_threshold item_file");
            return;
        }

        String trainingFile = args[0];
        String testFile = args[1];
        int cutoff = Integer.parseInt(args[2]);
        String recommendationFile = args[3];
        // For non personalized recommenders, this has to be set to true, as they will
        // provide recommendations for new users
        boolean completeIndexes = Boolean.parseBoolean(args[4]);
        double relThres = Double.parseDouble(args[5]);
        int nItems = 200;
        String itemFile = args[6];

        final TimestampCalculator<Long, Long> its = new TimestampCalculator<>();
        its.computeInteractionTimes(trainingFile, lp, "\t", 1, 3);
        // only if metadata timestamps (such as release dates) are available in the
        // provided file
        if (new File(itemFile).exists()) {
            its.computeMetadataTimes(itemFile, lp, "\t", 0, 1);
        }

        // Recommendation part
        // Check if recommender file exists. If it exists, then we will only evaluate
        final Tuple4<List<Long>, List<Long>, List<Long>, List<Long>> indexes = retrieveTrainingTestIndexes(
                completeIndexes, trainingFile, testFile, lp, lp);
        final List<Long> usersTraining = indexes.v1;
        final List<Long> itemsTraining = indexes.v2;
        final List<Long> usersTest = indexes.v3;
        final List<Long> itemsTest = indexes.v4;

        final FastUserIndex<Long> userIndexTraining = SimpleFastUserIndex.load(usersTraining.stream());
        final FastItemIndex<Long> itemIndexTraining = SimpleFastItemIndex.load(itemsTraining.stream());

        final FastUserIndex<Long> userIndexTest = SimpleFastUserIndex.load(usersTest.stream());
        final FastItemIndex<Long> itemIndexTest = SimpleFastItemIndex.load(itemsTest.stream());

        final FastPreferenceData<Long, Long> trainingPrefData = SimpleFastPreferenceData.load(
                SimpleRatingPreferencesReader.get().read(trainingFile, lp, lp), userIndexTraining, itemIndexTraining);

        final FastPreferenceData<Long, Long> testPrefData = SimpleFastPreferenceData
                .load(SimpleRatingPreferencesReader.get().read(testFile, lp, lp), userIndexTest, itemIndexTest);

        File file = new File(recommendationFile);
        if (!file.exists()) {
            System.out.println("File does not exist, computing recommender file");
            final Recommender<Long, Long> popRecommender = new PopularityRecommender(trainingPrefData);
            final Recommender<Long, Long> skyPerfRecommender = new SkylineRecommender<>(testPrefData);
            final Recommender<Long, Long> skyFreshRecommender = new SkylineFreshnessRecommender<>(trainingPrefData, its, MetricScheme.LAST);
            // change the following line to generate a different recommender
            // (not the best way, but it is simpler...)
            final Recommender<Long, Long> ranksysRecommender = popRecommender;
            writeRecommendation(testPrefData, trainingPrefData, ranksysRecommender, nItems, recommendationFile);
        } else {
            System.out.println("Recommended file exists, going directly to evaluation");
        }

        // Evaluation part
        final PreferenceData<Long, Long> trainingDataForEvaluation = trainingPrefData;

        final Map<String, RecommendationMetric<Long, Long>> recMetrics = new HashMap<>();
        final RelevanceModel<Long, Long> relevanceModel = new BinaryRelevanceModel<>(false, testPrefData, relThres);
        final RelevanceModel<Long, Long> noRelevanceModel = new NoRelevanceModel<>();
        final RankingDiscountModel discModel = new NoDiscountModel();

        recMetrics.put("ain_simplenorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.AVG),
                relevanceModel, discModel));
        recMetrics.put("fin_simplenorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.FIRST),
                relevanceModel, discModel));
        recMetrics.put("min_simplenorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.MEDIAN),
                relevanceModel, discModel));
        recMetrics.put("lin_simplenorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.LAST),
                relevanceModel, discModel));

        recMetrics.put("ain_minmaxnorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.AVG),
                relevanceModel, discModel));
        recMetrics.put("fin_minmaxnorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.FIRST),
                relevanceModel, discModel));
        recMetrics
                .put("min_minmaxnorm",
                        new GenericFreshness<>(
                                cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                        FreshnessMetricNorm.MINMAXNORM, MetricScheme.MEDIAN),
                                relevanceModel, discModel));
        recMetrics.put("lin_minmaxnorm", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.LAST),
                relevanceModel, discModel));

        recMetrics.put("ain_simplenorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.AVG),
                noRelevanceModel, discModel));
        recMetrics.put("fin_simplenorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.FIRST),
                noRelevanceModel, discModel));
        recMetrics.put("min_simplenorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.MEDIAN),
                noRelevanceModel, discModel));
        recMetrics.put("lin_simplenorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.NO_NORM, MetricScheme.LAST),
                noRelevanceModel, discModel));

        recMetrics.put("ain_minmaxnorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.AVG),
                noRelevanceModel, discModel));
        recMetrics.put("fin_minmaxnorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.FIRST),
                noRelevanceModel, discModel));
        recMetrics
                .put("min_minmaxnorm_norel",
                        new GenericFreshness<>(
                                cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                        FreshnessMetricNorm.MINMAXNORM, MetricScheme.MEDIAN),
                                noRelevanceModel, discModel));
        recMetrics.put("lin_minmaxnorm_norel", new GenericFreshness<>(cutoff,
                new ItemFreshness<>(trainingDataForEvaluation, its, FreshnessMetricNorm.MINMAXNORM, MetricScheme.LAST),
                noRelevanceModel, discModel));

        if (new File(itemFile).exists()) {
            recMetrics
                    .put("yin_simplenorm",
                            new GenericFreshness<>(
                                    cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                            FreshnessMetricNorm.NO_NORM, MetricScheme.FIRST_RELEASE),
                                    relevanceModel, discModel));
            recMetrics
                    .put("yin_minmaxnorm",
                            new GenericFreshness<>(
                                    cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                            FreshnessMetricNorm.MINMAXNORM, MetricScheme.FIRST_RELEASE),
                                    relevanceModel, discModel));
            recMetrics
                    .put("yin_simplenorm_norel",
                            new GenericFreshness<>(
                                    cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                            FreshnessMetricNorm.NO_NORM, MetricScheme.FIRST_RELEASE),
                                    noRelevanceModel, discModel));
            recMetrics
                    .put("yin_minmaxnorm_norel",
                            new GenericFreshness<>(
                                    cutoff, new ItemFreshness<>(trainingDataForEvaluation, its,
                                            FreshnessMetricNorm.MINMAXNORM, MetricScheme.FIRST_RELEASE),
                                    noRelevanceModel, discModel));
        }

        recMetrics.put("P", new Precision<>(cutoff, relevanceModel));
        recMetrics.put("NDCG", new NDCG<>(cutoff, new NDCG.NDCGRelevanceModel<>(false, testPrefData, relThres)));

        final PreferenceData<Long, Long> recommendedData = SimplePreferenceData
                .load(SimpleRatingPreferencesReader.get().read(recommendationFile, lp, lp));
        final int numUsersRecommended = recommendedData.numUsersWithPreferences();
        final int numUsersTest = testPrefData.numUsersWithPreferences();

        final Map<String, SystemMetric<Long, Long>> sysMetrics = new HashMap<>();
        recMetrics.forEach((name, metric) -> sysMetrics.put(name + "@" + cutoff,
                new AverageRecommendationMetric<>(metric, numUsersRecommended)));
        sysMetrics.put("USC", new SystemMetric<Long, Long>() {
            private Set<Long> users = new TreeSet<>();

            @Override
            public void add(Recommendation<Long, Long> arg0) {
                users.add(arg0.getUser());
            }

            @Override
            public void combine(SystemMetric<Long, Long> arg0) {
            }

            @Override
            public double evaluate() {
                return 1.0 * users.size() / numUsersTest;
            }

            @Override
            public void reset() {
                users.clear();
            }
        });

        final RecommendationFormat<Long, Long> format = new SimpleRecommendationFormat<>(lp, lp);
        format.getReader(recommendationFile).readAll()
                .forEach(rec -> sysMetrics.values().forEach(metric -> metric.add(rec)));
        sysMetrics.forEach((name, metric) -> System.out.println(name + "\tall\t" + metric.evaluate()));
    }

    /**
     *
     * Method that writes recommendations for users in test into a file.
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param testData the test data
     * @param trainingData the training data
     * @param rec the recommender
     * @param numberItemsRecommend the maximum number of items to be recommended
     * per user
     * @param outputRecFile output file where recommendations will be printed
     * @throws FileNotFoundException when the output file could not be created
     */
    private static <U, I> void writeRecommendation(PreferenceData<U, I> testData, PreferenceData<U, I> trainingData,
            Recommender<U, I> rec, int numberItemsRecommend, String outputRecFile) throws FileNotFoundException {
        try (PrintStream out = new PrintStream(outputRecFile)) {
            Stream<U> targetUsers = testData.getUsersWithPreferences();
            // Only make recommendations for the users in test
            targetUsers.forEach(user -> {
                if (testData.getUserPreferences(user).count() != 0L && trainingData.containsUser(user)) {
                    Recommendation<U, I> recommendation = rec.getRecommendation(user, numberItemsRecommend,
                            trainItemsPredicate(user, trainingData));
                    if (recommendation == null || recommendation.getItems() == null) {
                        System.out.println("Recommendation for user " + user + " is null");
                    } else {
                        // Items recommended
                        AtomicInteger rank = new AtomicInteger(1);
                        recommendation.getItems().forEach(tup -> {
                            if (rank.get() <= numberItemsRecommend) {
                                out.println(user + "\t" + tup.v1 + "\t" + tup.v2 + "\t" + rank.get());
                                rank.incrementAndGet();
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     *
     * Method that returns the users and items lists for training and test
     * splits.
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param completeIndexes flag to denote if an aggregated index should be
     * created for training and test, or if separate indexes should be returned
     * @param trainingFile file with the training file
     * @param testFile file with the test set
     * @param up parser of users
     * @param ip parser of items
     * @return a tuple of users and items from training, and users and items
     * from test
     */
    private static <U, I> Tuple4<List<U>, List<I>, List<U>, List<I>> retrieveTrainingTestIndexes(
            boolean completeIndexes, String trainingFile, String testFile, Parser<U> up, Parser<I> ip) {
        List<U> usersTraining = null;
        List<I> itemsTraining = null;
        List<U> usersTest = null;
        List<I> itemsTest = null;
        if (completeIndexes) {
            Tuple2<List<U>, List<I>> userItems = getCompleteUserItems(trainingFile, testFile, up, ip);
            usersTraining = userItems.v1;
            itemsTraining = userItems.v2;
            usersTest = userItems.v1;
            itemsTest = userItems.v2;
        } else {
            Tuple2<List<U>, List<I>> userItemsTrain = getUserItemsFromFile(trainingFile, up, ip);
            usersTraining = userItemsTrain.v1;
            itemsTraining = userItemsTrain.v2;
            Tuple2<List<U>, List<I>> userItemsTest = getUserItemsFromFile(testFile, up, ip);
            usersTest = userItemsTest.v1;
            itemsTest = userItemsTest.v2;
        }
        return new Tuple4<>(usersTraining, itemsTraining, usersTest, itemsTest);
    }

    /**
     *
     * Method to obtain the user and the items from the complete training and
     * test data
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param fileTraining the training file
     * @param fileTest the test file
     * @param up the user parser
     * @param ip the item parser
     * @return a tuple with the list of users and items
     */
    public static <U, I> Tuple2<List<U>, List<I>> getCompleteUserItems(String fileTraining, String fileTest,
            Parser<U> up, Parser<I> ip) {
        try {
            PreferenceData<U, I> trainingData = SimplePreferenceData
                    .load(SimpleRatingPreferencesReader.get().read(fileTraining, up, ip));
            PreferenceData<U, I> testData = SimplePreferenceData
                    .load(SimpleRatingPreferencesReader.get().read(fileTest, up, ip));
            PreferenceData<U, I> totalData = new ConcatPreferenceData<>(trainingData, testData);
            List<U> usersList = totalData.getAllUsers().collect(Collectors.toList());
            List<I> itemsList = totalData.getAllItems().collect(Collectors.toList());

            return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
                    itemsList.stream().sorted().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * Method to obtain a tuple of users and items from a given file
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param file the file
     * @param up the user parser
     * @param ip the item parser
     * @return a tuple with the list of users and items
     */
    public static <U, I> Tuple2<List<U>, List<I>> getUserItemsFromFile(String file, Parser<U> up, Parser<I> ip) {
        try {
            PreferenceData<U, I> data = SimplePreferenceData
                    .load(SimpleRatingPreferencesReader.get().read(file, up, ip));
            List<U> usersList = data.getAllUsers().collect(Collectors.toList());
            List<I> itemsList = data.getAllItems().collect(Collectors.toList());

            return new Tuple2<>(usersList.stream().sorted().collect(Collectors.toList()),
                    itemsList.stream().sorted().collect(Collectors.toList()));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Predicate used in RankSys to make recommendations not to be in the
     * training set
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param user the user
     * @param trainingData the training set
     * @return a predicate
     */
    private static <U, I> Predicate<I> isNotInTraining(U user, PreferenceData<U, I> trainingData) {
        return p -> trainingData.getUserPreferences(user).noneMatch(itemPref -> itemPref.v1.equals(p));
    }

    /**
     *
     * Predicate that will return true if the item has been rated in the
     * training set
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param trainingData the training data
     * @return a predicate
     */
    private static <U, I> Predicate<I> itemWithRatingsInTraining(PreferenceData<U, I> trainingData) {
        return p -> trainingData.getItemPreferences(p).findFirst().isPresent();
    }

    /**
     *
     * Training items strategy as a predicate:
     *
     * -Only items with at least one rating in training should be recommended.
     * -Only items that the user has not rated in training (new items for that
     * user).
     *
     * @param <U> type of users
     * @param <I> type of items
     * @param user the user
     * @param trainingData the training data
     * @return a predicate
     */
    private static <U, I> Predicate<I> trainItemsPredicate(U user, PreferenceData<U, I> trainingData) {
        return isNotInTraining(user, trainingData).and(itemWithRatingsInTraining(trainingData));
    }

}
