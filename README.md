# TimeAwareNoveltyMetrics

# README #

This repository includes the time-aware novelty metrics defined in the following paper:

[1] Pablo Sánchez, Alejandro Bellogín. Time-aware novelty metrics. In ECIR 2018.


### Prerequisites

Maven

Java8

### Dependencies

[RankSys 0.4.3](https://github.com/RankSys/RankSys/releases/tag/0.4.3)


### Instructions ###

* Steps to install the library and download all the data and scripts:
    * git clone https://bitbucket.org/PabloSanchezP/timeawarenoveltymetrics
    * cd timeawarenoveltymetrics
    * mvn install
    * The generated JAR is in target\TimeAwareNoveltyMetrics-0.0.1-SNAPSHOT.jar

* If you want to use the library inside your (Java) project:
    * Add the JitPack dependency as described in [https://jitpack.io/#org.bitbucket.PabloSanchezP/timeawarenoveltymetrics](https://jitpack.io/#org.bitbucket.PabloSanchezP/timeawarenoveltymetrics)

* Running tests: check the scripts included in the repository:
    * [run_movielens20m.sh](./scripts/run_movielens20m.sh)
    * [run_epinions.sh](./scripts/run_epinions.sh)
    * [run_movietweetings600k.sh](./scripts/run_movietweetings600k.sh)


## Additional algorithms/frameworks used in the experiments
  * [MyMedialite](http://www.mymedialite.net/) (for BPR)
  * We have adapted the following approach for ranking evaluation:
    * [Fusing Similarity Models with Markov Chains for Sparse Sequential Recommendation](https://cseweb.ucsd.edu/~jmcauley/pdfs/icdm16a.pdf) (for Fossil, MC and FPMC) [Original source code](https://sites.google.com/view/ruining-he/)


## Authors

 * **Pablo Sánchez** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)
 * **Alejandro Bellogín** - [Universidad Autónoma de Madrid](https://uam.es/ss/Satellite/en/home.htm)

## Contact

 * **Pablo Sánchez** - <pablo.sanchezp@uam.es>

## License
This project is licensed under [the GNU GPLv3 License](LICENSE.txt)

## Acknowledgments

* This work was funded by the research project TIN2016-80630-P (MINECO)
