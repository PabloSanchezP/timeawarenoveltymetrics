#!/bin/sh

JAR=../target/TimeAwareNoveltyMetrics-0.0.1-SNAPSHOT.jar

# run recommender and evaluate it
java -jar $JAR ../data/MovieTweetings600K-5core/MovieTweetings_600K.train ../data/MovieTweetings600K-5core/MovieTweetings_600K.test 5 mt_itempop.rec true 9 ../data/MovieTweetings600K_releasedates.txt

