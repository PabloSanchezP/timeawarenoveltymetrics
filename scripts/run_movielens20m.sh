#!/bin/sh

JAR=../target/TimeAwareNoveltyMetrics-0.0.1-SNAPSHOT.jar

percentage_training=0.8

if [ ! -f ml-20m.zip ]; then
	wget http://files.grouplens.org/datasets/movielens/ml-20m.zip
	unzip ml-20m.zip
fi

# process the rating file
if [ ! -f ratings.dat ]; then
	tail -n +2 ml-20m/ratings.csv | tr "," "\t" | sort -t'	' -n -k4,4 > ratings.dat
fi
nratings=`wc -l ratings.dat | cut -d ' ' -f 1`
training_size=`echo $nratings $percentage_training | awk '{print int($1 * $2)}'`
difference=`echo $nratings $training_size | awk '{print $1 - $2}'`
# training
head -$training_size ratings.dat > ml20training.dat
# test
tail -$difference ratings.dat > ml20test.dat

# run recommender and evaluate it
java -jar $JAR ml20training.dat ml20test.dat 5 ml20_itempop.rec true 5 ../data/MovieLens20M_releasedates.txt

